﻿using System;
using System.Collections.Generic;

namespace ExcelImport
{
    /// <summary>
    /// Stores raw data from worksheet.
    /// </summary>
    public class VariantImportData
    {
        public IEnumerable<string> Header;
        public IEnumerable<VariantImportDataRow> Rows;

        public VariantImportData(IEnumerable<string> header, IEnumerable<VariantImportDataRow> rows)
        {
            Header = header;
            Rows = rows;
        }
    }

    /// <summary>
    /// Stores a datarow for one Variant.
    /// </summary>
    public class VariantImportDataRow
    {
        public IEnumerable<object> Content;
        public Type DataType; // might be useless?

        public VariantImportDataRow(IEnumerable<object> content)
        {
            Content = content;
            DataType = null;
        }

        public VariantImportDataRow(IEnumerable<string> content, Type dataType)
        {
            Content = content;
            DataType = dataType;
        }
    }
}
