﻿namespace ExcelImport
{
    public class VariantImportConfiguration
    {
        #region Properties
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
        public virtual int? Version { get; set; }

        public virtual string ExcelPath { get; set; }

        public virtual int? ExcelSheet { get; set; } // Defaults to frist worksheet in excelfile

        public virtual long VariantDefinitionId { get; set; }

        public virtual int HeaderRow { get; set; }
        public virtual int HeaderStartColumn { get; set; }
        public virtual int HeaderEndColumn { get; set; }
        public virtual int DataStartRow { get; set; }

        public virtual int? MappingId { get; set; }
        #endregion
    }
}