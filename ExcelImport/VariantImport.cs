﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace ExcelImport
{
    /// <summary>
    /// Handles import of excel files turning it into a VariantImportData class object for further use.
    /// </summary>
    public class VariantImport
    {
        private readonly VariantImportConfiguration Config;

        /// <summary>
        /// Initializing import with a config and later on with a mapping
        /// </summary>
        /// <param name="config">Configurationfile provided by user</param>
        public VariantImport(VariantImportConfiguration config)
        {
            Config = config;
        }

        /// <summary>
        /// Transforms worksheet into VariantImportData class object for further use.
        /// </summary>
        public VariantImportData GetDataFromExcel()
        {
            Application app = null;
            Workbooks books = null;
            Workbook book = null;
            Sheets sheets = null;
            Worksheet sheet = null;
            Range headerRange = null;
            Range variantsRange = null;

            try
            {
                app = new Application
                {
                    Visible = false,
                    DisplayAlerts = false
                };
                books = app.Workbooks;
                book = books.Open(Config.ExcelPath);
                sheets = book.Sheets;
                sheet = sheets[Config.ExcelSheet];

                int lastrow = LastRow(sheet);
                headerRange = sheet.Range[sheet.Cells[Config.HeaderRow, Config.HeaderStartColumn], sheet.Cells[Config.HeaderRow, Config.HeaderEndColumn]];
                variantsRange = sheet.Range[sheet.Cells[Config.DataStartRow, Config.HeaderStartColumn], sheet.Cells[lastrow, Config.HeaderEndColumn]];

                Array header = headerRange.Cells.Value;
                Array variants = variantsRange.Cells.Value;

                Dictionary<int, string> headerDict = new Dictionary<int, string>();

                for (int i = 1; i <= header.Length; i++)
                {
                    if(header.GetValue(Config.HeaderRow,i) != null) headerDict.Add(i, header.GetValue(Config.HeaderRow,i).ToString());
                }

                var rows = new List<VariantImportDataRow>();
                for (int i = 1; i <= (lastrow - Config.DataStartRow) + 1; i++)
                {
                    var content = new List<object>();
                    foreach (var col in headerDict)
                    {
                        if (variants.GetValue(i, col.Key) != null)
                            content.Add(variants.GetValue(i, col.Key).ToString());
                        else
                            content.Add("");
                    }
                    rows.Add(new VariantImportDataRow(content));
                }

                book.Close();
                app.Quit();

                return new VariantImportData(headerDict.Values.ToList(), rows);
            } // catch ?
            finally
            {
                if (variantsRange != null) Marshal.ReleaseComObject(variantsRange);
                if (headerRange != null) Marshal.ReleaseComObject(headerRange);
                if (sheet != null) Marshal.ReleaseComObject(sheet);
                if (sheets != null) Marshal.ReleaseComObject(sheets);
                if (book != null) Marshal.ReleaseComObject(book);
                if (books != null) Marshal.ReleaseComObject(books);
                if (app != null) Marshal.ReleaseComObject(app);
            }
        }

        /// <summary>
        /// Returns the last nonempty row in the worksheet. Maybe to unreliable? (e.g. blank character)
        /// </summary>
        private int LastRow(Worksheet sheet)
        {
            object misValue = Missing.Value;
            int sum = 0;
            Range count = null;
            count = sheet.UsedRange.Columns[1, misValue] as Range;
            foreach (Range cell in count.Cells)
            {
                sum += 1;
            }
            if (count != null) Marshal.ReleaseComObject(count);
            return sum;
        }
    }
}
