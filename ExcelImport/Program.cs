﻿using System;
using System.Linq;

namespace ExcelImport
{
    class Program
    {
        static void Main()
        {
            VariantImportConfiguration config = new VariantImportConfiguration();
            
            #region Simulated Config
            //config.ExcelPath = @"C:\Users\Dragan\Desktop\AVL\VariantImport\ImportTest.xlsx";
            config.ExcelPath = @"C:\Users\Dragan\Desktop\AVL\VariantImport\2016_A573010-Projektplanung-PowertrainDB_Tabelle_1017_Import.xlsx";
            config.ExcelSheet = 4;
            config.DataStartRow = 4;
            config.HeaderRow = 1;
            config.HeaderStartColumn = 11;
            config.HeaderEndColumn = 53;
            #endregion

            VariantImport import = new VariantImport(config);
            VariantImportData data = import.GetDataFromExcel();
            
            int count = 0;

            foreach (var row in data.Rows)
            {
                Console.WriteLine($"------------[Variant #: {count}]------------------");
                count++;
                for (int i = 0; i < data.Header.Count(); i++)
                {
                    Console.WriteLine("Property " + data.Header.ToList()[i] + ": " + row.Content.ToList()[i]);
                }
            }

            Console.ReadLine();
        }
    }
}
